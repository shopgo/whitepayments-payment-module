<?php

class Shopgo_Whitepayments_Model_Payment extends Mage_Payment_Model_Method_Cc
{
    protected $_code  = 'whitepayments';

    /**
     * Availability options
     */
    protected $_isGateway               = true;
    protected $_canAuthorize            = false;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = true;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = true;
    protected $_canSaveCc				= false;
    protected $_isInitializeNeeded      = false;

    // protected $_allowCurrencyCode = array('USD');

    // protected $_formBlockType = 'whitepayments/form_paymentform';

    private $errorDetails;

	/**
     * Check method for processing with base currency
     *
     * @param string $currencyCode
     * @return boolean
     */
    // public function canUseForCurrency($currencyCode)
    // {
    //     if (!in_array($currencyCode, $this->_allowCurrencyCode)) {
    //         return false;
    //     }
    //     return true;
    // }

	/**
     * this method is called if we are just authorising
     * a transaction
     */
    public function authorize (Varien_Object $payment, $amount)
    {
        //
        //return $this;
    }

    /**
     * this method is called if we are authorising AND
     * capturing a transaction
     */
    public function capture (Varien_Object $payment, $amount)
    {
        require_once(Mage::getBaseDir('lib') . DS . 'Shopgo' . DS . 'White' . DS . 'White.php');

        /**
         * The new API uses the amount in cents, not in decimals. That means
         * and amount of 1000 = AED 10.00
         *
         * Since we only support AED/USD at the moment, both of which have 2
         * decimal points, we'll multiply the amount by 100.
         */
        $amount_in_cents = round($amount*100, 0);

        $order = $payment->getOrder();
        $billing = $payment->getOrder()->getBillingAddress();
        $shipping = $order->getShippingAddress();

        $payment_response = array();
        $error = false;

        try {
            // Use White's bindings...
            White::setApiKey(Mage::helper('core')->decrypt($this->getConfig('secret_key')));

            $payment_response = White_Charge::create(array(
              "amount" => $amount_in_cents, // needs to be converted to cents
              "currency" => $order->getBaseCurrencyCode(),
              "card" => array(
                "number" => $payment->getCcNumber(),
                "exp_month" => $payment->getCcExpMonth(),
                "exp_year" => $payment->getCcExpYear(),
                "cvc" => $payment->getCcCid()
              ),
              "description" => "Charge for order ID " . $order->getIncrementId()
            ));

            // Error checking not necessary -- errors are thrown as Exceptions
            // If you did want to check anyway, use the 'state' for testing...
            if($payment_response['state'] == 'captured') {
                // Success!
            }

        } catch(White_Error_Banking $e) {
            // Since it's a decline, White_Error_Banking will be caught
            $error = Mage::helper('whitepayments')->__('Gateway request error: %s', $e->getMessage());
        } catch (White_Error_Request $e) {
            // Invalid parameters were supplied to White's API
            $error = Mage::helper('whitepayments')->__('Gateway request error: %s', $e->getMessage());
        } catch (White_Error $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $error = Mage::helper('whitepayments')->__('Gateway request error: %s', $e->getMessage());
        } catch (Exception $e) {
            // Something else happened, completely unrelated to White
            $error = Mage::helper('whitepayments')->__('Gateway request error: %s', $e->getMessage());
        }

        if ($error !== false) {
            Mage::throwException($error);
        } else {
            $payment->setStatus(self::STATUS_APPROVED);
            $payment->setLastTransId($payment_response['id']);
        }

        return $this;
    }

    /**
     * called if refunding
     */
    public function refund (Varien_Object $payment, $amount)
    {
        // Refund is now supported -- implementation will be done in a separate PR
        Mage::throwException("Refund not Supported.");
        return $this;
    }

    /**
     * called if voiding a payment
     */
    public function void (Varien_Object $payment)
    {
        // Void is supported too .. Again, will be done in a separate PR
        Mage::throwException("Void not Supported.");
        return $this;
    }

    public function getConfig($path) {
        return Mage::getStoreConfig('payment/whitepayments/' . $path);
    }
}