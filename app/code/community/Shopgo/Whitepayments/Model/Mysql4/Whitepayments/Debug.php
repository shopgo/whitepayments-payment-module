<?php

class Shopgo_Whitepayments_Model_Mysql4_Whitepayments_Debug extends Mage_Core_Model_Mysql4_Abstract
{
	protected function _construct()
	{
		$this->_init('whitepayments/whitepayments_debug', 'debug_id');
	}
}