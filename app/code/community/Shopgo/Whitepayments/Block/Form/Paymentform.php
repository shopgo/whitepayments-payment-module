<?php

class Shopgo_Whitepayments_Block_Form_Paymentform extends Mage_Payment_Block_Form_Cc
{
    function _construct()
	{
		parent::_construct();
		$this->setTemplate('shopgo/whitepayments/form/paymentform.phtml');
	}
}
?>